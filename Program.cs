﻿using System;
using System.IO;
using System.Collections.Generic;

namespace LogAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            List<DataInfo> datalogs = new List<DataInfo>();

            if(args.Length == 0)
            {
                Console.WriteLine("Please pass a logfile when running this through CMD");
                Console.WriteLine("Ex. LogAnalyzer.exe H8ful-backup-2021.04.06-10.36.00.log");
                return;
            }
            StreamReader file = new StreamReader(args[0]);
            if(file == null)
            {
                Console.WriteLine("File not found");
            }
            else
            {
                DateTime lastLevelLoad = DateTime.MinValue;
                
                int lineNum = 0;

                string line = file.ReadLine();
                while(line != null)
                {
                    if (line.Contains("[DATALOG]:"))
                    {
                        // Found datalog, get all the rows for it
                        int dataLineNum = lineNum;
                        int dataLength = 1;
                        string data = line;

                        line = NextLine(file, ref lineNum);
                        while(line[0] != '[')
                        {
                            data += "\n" + line;
                            dataLength++;
                            line = NextLine(file, ref lineNum);
                        }
                        
                        // Create a DataInfo and log it in
                        
                        DataInfo info = new DataInfo(dataLineNum, data);
                        datalogs.Add(info);

                        if(info.dataType == DataInfo.TYPE.LEVELLOAD)
                        {
                            if(lastLevelLoad == DateTime.MinValue)
                            {
                                lastLevelLoad = info.time;
                            }
                            else
                            {
                                info.levelDuration = info.time.Subtract(lastLevelLoad);
                                lastLevelLoad = info.time;
                            }
                        }
                    }
                    else if(line.Contains("LogInit: OS"))
                    {
                        DataInfo info = new DataInfo(lineNum, line);
                        info.dataType = DataInfo.TYPE.HARDWARE;
                        datalogs.Add(info);
                        line = NextLine(file, ref lineNum);
                    }
                    else 
                    {
                        line = NextLine(file, ref lineNum);
                    }
                }

                List<DataInfo> fpsinfo = datalogs.FindAll(e => e.dataType == DataInfo.TYPE.SETTINGS
                                                            || e.dataType == DataInfo.TYPE.FPS
                                                            || e.dataType == DataInfo.TYPE.LEVELLOAD
                                                            || e.dataType == DataInfo.TYPE.HARDWARE);

                Console.WriteLine("Found lines: " + fpsinfo.Count);

                foreach(DataInfo di in fpsinfo)
                {
                    di.PrintResult();
                }
            }
        }

        static string NextLine(StreamReader file, ref int linenum)
        {
            linenum++;
            return file.ReadLine();
        }
    }
}
