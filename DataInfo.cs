using System;

namespace LogAnalyzer
{
    class DataInfo
    {
        int row;
        public DateTime time;
        public TimeSpan levelDuration = TimeSpan.MinValue;
        public string data;

        public enum TYPE {NULL, LEVELLOAD, LEVELSCORE, BUILDINGSCORE, FPS, SETTINGS, HARDWARE, DMG}
        public TYPE dataType = TYPE.NULL;
        public DataInfo(int _row, string _data)
        {
            row = _row;
            data = _data;
            Init();
        }

        public virtual void Init()
        {
            time = getTime(data);
            InitType();
        }

        public virtual void PrintResult()
        {
            if(dataType == TYPE.LEVELLOAD && levelDuration != TimeSpan.MinValue) Console.WriteLine("\nLevel Completed in ({0}h {1}min {2}s)", levelDuration.Hours,levelDuration.Minutes, levelDuration.Seconds); 
            Console.Write("\n[{0}]{1}\n" ,time.ToLongTimeString(), data);
        }

        public DateTime getTime(string line)
        {
            int curCharPos = 0;

            DateTime time;

            if(line[curCharPos] == '[')
            {
                while(line[curCharPos] != ']')
                {
                    curCharPos++;
                }
                string dateline = line.Substring(0,curCharPos);

                int years, months, days, hours, minutes, seconds, milliseconds = -1;
                years = int.Parse(dateline.Substring(1,4));
                months = int.Parse(dateline.Substring(6,2));
                days = int.Parse(dateline.Substring(9,2));
                hours = int.Parse(dateline.Substring(12,2));
                minutes = int.Parse(dateline.Substring(15,2));
                seconds = int.Parse(dateline.Substring(18,2));
                milliseconds = int.Parse(dateline.Substring(21,3));
                // [2021.04.06-10.27.12:066]
                //  yyyy.mm.dd - hh.mm.ss.ms
                time = new DateTime(years, months, days, hours, minutes, seconds, milliseconds);
            }
            else
            {
                return DateTime.MinValue;
            }

            return time;
        }

        public void InitType()
        {
            // [DataManager] [DATALOG]: VideoSettings Loaded:
            // Current Resolution:1152 x 864
            // Graphical Quality: Ultra
            // PostProcessing: Ultra
            // Anti-Aliasing: High
            // Shadow Quality: High
            // Max Framerate: 90 Hz
            // Destruction Quality:
            if(data.Contains("VideoSettings"))
            {
                dataType = TYPE.SETTINGS;
                data = data.Substring(80);
            }
            // [DataManager] Cur FPSAVG 79.256424
            // [DataManager] [DATALOG]:
            // Framerates during level:
            //     AVG: 79.256424
            //     Min: 62.424702
            //     Max: 89.999283
            else if(data.Contains("Framerates"))
            {
                dataType = TYPE.FPS;
                data = data.Substring(82);
            }
            // [DataManager] [DATALOG]: BP_NPC10 Damaged Player for ( 20.0 )
            //     Current Health: 60.0
            else if(data.Contains("Damaged Player for"))
            {
                dataType = TYPE.DMG;
            }
            // [2021.04.06-10.12.03:034][723]LogBlueprintUserMessages: [DataManager] [DATALOG]: Loading level: TVCorp_Level_02 ( 18/22 )
            else if(data.Contains("Loading level:"))
            {
                dataType = TYPE.LEVELLOAD;
                data = data.Substring(89);
            }
            else
            {
                dataType = TYPE.NULL;
            }

        }
    }
}